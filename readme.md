Simple Django starter package with user authentication, registration, JWT support.


Installation
------------

- git clone https://gitlab.com/Madrize/django-starter
- rm -r .git && git init
- cd djang-starter
- touch djangostarter/local_settings.py
- python manage.py runserver


Installed Packages
------------------

- https://github.com/marcgibbons/django-rest-swagger
- https://github.com/Tivix/django-rest-auth
- https://github.com/pennersr/django-allauth
- http://getblimp.github.io/django-rest-framework-jwt/